import React from 'react'
import './control-panel.css'
import NewCard from "./newCard/NewCard";
import Answer from "./answer/Answer";
import ShowResult from "./showResult/ShowResult";

export default class ControlPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shouldShow: false
    }
  }

  handlerShowAnswer() {
    clearTimeout();
    this.setState({
      shouldShow: true
    });
  }

  handlerClickNewCard() {
    this.props.newCard();
    this.setState({
      shouldShow: true
    });
    setTimeout(() => {
      this.setState({
        shouldShow: false
      });
    }, 3000)
  }

  render() {
    return (
      <div className='control-panel-class'>
        <NewCard clickNewCard={this.handlerClickNewCard.bind(this)}/>
        <Answer shouldShow={this.state.shouldShow} answer={this.props.answer}/>
        <ShowResult showAnswer={this.handlerShowAnswer.bind(this)}/>
      </div>
    )
  }
}
