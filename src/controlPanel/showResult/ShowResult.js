import React from 'react'
import './show-result.css'

const ShowResult = (props) => {
  return (
    <div className='show-result'>
      <button onClick={props.showAnswer}>show Result</button>
    </div>
  )
};
export default ShowResult;
