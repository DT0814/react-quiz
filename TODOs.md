Well：
* 状态提升
* 组件划分基本合理
* 对react类、状态的定义和维护已经基本理解

Need to improve：
* 考虑函数组件和类组件的使用场景： NewCard/Result/Guess中定义的state没有意义等
* 命名要更表意，newCard -> createCard， resultStr -> result
* 代码规范，js代码每一行都要有分号
* .gitignore不完整

Suggestions：
* 基本的react内容和概念没有问题，多关注在代码质量的提升就好