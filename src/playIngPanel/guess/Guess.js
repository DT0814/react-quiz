import React from 'react'
import './guess.css'
const Guess = (props) => {

  const handlerCheckAnswer = () => {
    const result = props.checkAnswer(props.answer);
    let resultText = result ? 'SUCCESS' : 'FAILED';
    props.changeResult(resultText, result);
  };

  const handlerChangeAnswer = (event) => {
    props.changeAnswer(event.target.value);
  };

  return (
    <div className='guess-class'>
      <label>Guess Card</label>
      <br/>
      <div className='guess-class-body'>
        <input type="text" onChange={handlerChangeAnswer.bind(this)} value={props.answer}
               className='answer-input'/>
        <br/>
        <button onClick={handlerCheckAnswer.bind(this)}>
          Guess
        </button>
      </div>
    </div>
  )
};
export default Guess;
