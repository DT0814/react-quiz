import React from 'react'
import './new-card.css'

const NewCard = (props) => {
  return (
    <div className='new-card'>
      <label>New Card</label>
      <br/>
      <button onClick={props.clickNewCard}>New Card</button>
    </div>
  )
};
export default NewCard;
