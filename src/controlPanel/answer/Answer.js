import React from 'react'
import './answer.css'

const Answer = (props) => {
  return (
    <div>{
      props.shouldShow ?
        <div className='right-answer'>
          <label className='answer-label'>{props.answer[0]}</label>
          <label className='answer-label'>{props.answer[1]}</label>
          <label className='answer-label'>{props.answer[2]}</label>
          <label className='answer-label'>{props.answer[3]}</label>
        </div>
        :
        <div className='right-answer'></div>
    }
    </div>
  )
};
export default Answer;
