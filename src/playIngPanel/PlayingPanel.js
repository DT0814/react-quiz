import React from 'react'
import './playing-panel.css'
import Result from "./result/Result";
import Guess from "./guess/Guess";

export default class PlayingPanel extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      answer: [],
      resultText: ''
    }
  }

  handlerClearState() {
    this.setState({
      answer: [],
      resultText: '',
      right: false
    })
  }

  handlerChangeAnswer(answer) {
    let reg = /^[A-Za-z]+$/;
    if (answer.length > 4 || !reg.test(answer)) {
      alert('只能输入四位字母');
      return;
    }

    this.setState({
      answer: answer.toUpperCase()
    })
  }

  handlerChangeResult(resultText, right) {
    this.setState({
      resultText: resultText,
      right: right
    })
  }

  render() {
    return (
      <div className='playing-panel-class'>
        <Result
          right={this.state.right}
          answer={this.state.answer}
          resultStr={this.state.resultText}/>
        <Guess
          answer={this.state.answer}
          checkAnswer={this.props.checkAnswer}
          changeAnswer={this.handlerChangeAnswer.bind(this)}
          changeResult={this.handlerChangeResult.bind(this)}/>
      </div>
    )
  }
}
