import React, { Fragment } from 'react';
import PlayingPanel from "./playIngPanel/PlayingPanel";
import ControlPanel from "./controlPanel/ControlPanel";
import './App.css'
import createRandomLetter from "./utils/createRandomLetter";

class App extends React.Component {


  constructor(props) {
    super(props);
    let answer = createRandomLetter();
    this.state = {
      answer: answer,
    }
  }

  checkAnswer(answer) {
    return answer === this.state.answer;
  }

  handlerNewCard() {
    let answer = createRandomLetter();
    this.setState({
      answer: answer
    });
    this.refs.playingPanel.handlerClearState();
  }

  render() {
    return (
      <Fragment>
        <PlayingPanel checkAnswer={this.checkAnswer.bind(this)} ref='playingPanel'/>
        <ControlPanel newCard={this.handlerNewCard.bind(this)} answer={this.state.answer}/>
      </Fragment>
    )
  };
}

export default App;
