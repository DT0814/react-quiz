import React from 'react'
import './result.css'

 const Result =(props)=> {

    return (
      <div className='result-class'>
        <label className='title-label'>Your Result</label>
        <div className='answer'>
          <label className='answer-label'>{props.answer[0]}</label>
          <label className='answer-label'>{props.answer[1]}</label>
          <label className='answer-label'>{props.answer[2]}</label>
          <label className='answer-label'>{props.answer[3]}</label>
        </div>
        {
          props.right
            ? <label className='result-label green'>{props.resultStr}</label>
            : <label className='result-label red'>{props.resultStr}</label>
        }

      </div>
    )
};
export default Result;
