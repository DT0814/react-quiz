const createRandomLetter = (size = 4) => {
  let res = new Set();
  while (res.size < size) {
    res.add(String.fromCharCode(65 + getRandom()))
  }
  return [...res].join('');
};

const getRandom = (size = 26) => {
  return Math.floor(Math.random() * Math.floor(size));
}
;
export default createRandomLetter;
